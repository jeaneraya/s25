/* JSON Object
	- JSON stands for Javascript Object Notation
	- JSON is also used for other programming languages hence the name Javascript Object Notation

	Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}
*/

// JSON Object

/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

// JSON Array

/*"cities" = [
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Manila", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Makaty City", "province": "Metro Manila", "country": "Philippines"}
]*/

//JSON Methods
/*
	The JSON Object contains methods for parsing and converting data into stringfied JSON
*/

//Converting data into Stringified JSON

let batchArr = [
	{batchName: "Batch 197"},
	{batchName: "Batch 198"}
];

console.log(batchArr);

// The stringified method is used to convert JS Objects into string
// We are doing this before sending the data to convert an array or an object to its string equivalent.

console.log("Result from stringify method:");
console.log(JSON.stringify(batchArr));

let userProfile = JSON.stringify({
	name: "Jean Victoria",
	age: 27,
	address: {
		city: "Kidapawan",
		region: "Sockssargen",
		country: "Philippines"
	}
})

console.log(`Result from stringify method ${userProfile}`);

/*let firstName = prompt("What is your firstname?");
let lastName = prompt("What is your lastName?");
let age = prompt("What is your age?");
let address = {
	city: prompt("Which City do you live in?"),
	country: prompt("Which Country does your city address belong?")
};
*/
/*let userData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})*/

//console.log(userData);

// Convert stringified JSON into JS Objects 
// JSON.parse()

let batchesJSON = `[
	{
		"batchName": "Batch 197"
	},
	{
		"batchName": "Batch 198"
	}
]`

console.log(batchesJSON);

// Upon receiving data, the JSON text can be converted into JS Objects so that we can use it in our program

console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{
	"name": "Trisha",
	"age": 20,
	"address": {
		"city": "Caloocan",
		"country": "Philippines"
	}
}`

console.log(stringifiedObject);
console.log("Result from parse method (object):");
console.log(JSON.parse(stringifiedObject));